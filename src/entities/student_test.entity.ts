import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, Unique, JoinColumn } from "typeorm"
import { Student } from "./student.entity";
import { Test } from "./test.entity";

@Entity()
export class Student_Test {

    @PrimaryGeneratedColumn("identity", { name: "StudentId" })
    @ManyToOne((_) => Student, { nullable: false })
    @JoinColumn({ name: "StudentId" })
    Students: Student;

    @PrimaryGeneratedColumn("identity", { name: "TestId" })
    @ManyToOne((_) => Test, { nullable: false })
    @JoinColumn({ name: "TestId" })
    Tests: Test;

    @Column({ type: "int", nullable: true })
    Score: number | null;

}