import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable, OneToMany, CreateDateColumn, Unique } from "typeorm"
import { Subject } from "./subject.entity";
import { Student_Subject } from "./student_subject.entity";
import { Student_Test } from "./student_test.entity";
import { Test } from "./test.entity";

@Entity()
@Unique("unique_student_code", ["Code"])
export class Student {
    @PrimaryGeneratedColumn("increment", {
        primaryKeyConstraintName: "pk_student",
    })
    Id: number;

    @Column({ type: "varchar", length: 255 })
    Code: string;

    @Column({ type: "varchar", length: 255, nullable: true })
    Name: string;

    @CreateDateColumn({
        nullable: true, default: () => "timezone('utc'::text, now())",
    })
    Birth: Date;

    @Column({ type: 'varchar', length: 255, nullable: true })
    Image: string;

    //@ManyToMany(() => Subject, student => student.Students)
    @JoinTable({
        name: "student_subject",
        joinColumn: { name: "StudentId", referencedColumnName: "Id" },
        inverseJoinColumn: { name: "SubjectId", referencedColumnName: "Id" },
    })
    Subjects: Subject[];

    @OneToMany(() => Student_Subject, student => student.Students)
    Student_Subjects: Student_Subject[];

    //@ManyToMany(() => Test, student => student.Students)
    @JoinTable({
        name: "student_test",
        joinColumn: { name: "StudentId", referencedColumnName: "Id" },
        inverseJoinColumn: { name: "TestId", referencedColumnName: "Id" },
    })
    Tests: Test[];

    @OneToMany(() => Student_Test, (temp) => temp.Students)
    Student_Tests: Student_Test[];

}