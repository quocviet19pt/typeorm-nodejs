import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne, Unique, JoinColumn } from "typeorm"
import { Subject } from "./subject.entity";
import { Test } from "./test.entity";

@Entity()
@Unique("unique_teacher_code", ["Code"])
export class Teacher {
    @PrimaryGeneratedColumn("increment", {
        primaryKeyConstraintName: "pk_teacher",
    })
    Id: number;

    @Column({ type: "varchar", length: 255, nullable: true })
    Code: string;

    @Column({ type: "varchar", length: 255, nullable: true })
    Name: string;

    @Column({ type: 'int', nullable: true })
    Exp: number;

    @Column({ type: 'text', nullable: true })
    Image: string;

    @OneToMany(() => Test, (test) => test.Teachers)
    Tests: Test[];

    @ManyToOne(() => Subject, (subject) => subject.Teachers)
    @JoinColumn({ name: "SubjectId" })
    Subjects: Subject

}