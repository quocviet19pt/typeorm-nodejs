import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm"
import { Subject } from "./subject.entity"
import { Student } from "./student.entity"


@Entity()
export class Student_Subject {
    @PrimaryGeneratedColumn("identity", { name: "StudentId" })
    @ManyToOne((_) => Student, { nullable: false })
    @JoinColumn({ name: "StudentId" })
    Students: Student;

    @PrimaryGeneratedColumn("identity", { name: "SubjectId" })
    @ManyToOne((_) => Subject, { nullable: false })
    @JoinColumn({ name: "SubjectId" })
    Subjects: Subject;
}