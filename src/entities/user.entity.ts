import { Entity, Column, PrimaryGeneratedColumn, Unique } from "typeorm"


@Entity()
@Unique("unique_User_Name", ["Name"])
export class User {
    @PrimaryGeneratedColumn("increment", {
        primaryKeyConstraintName: "pk_User",
    })
    Id: number;

    @Column({ type: "varchar", length: 255, nullable: true })
    Name: string;

    @Column({ type: "varchar", length: 255, nullable: true })
    Role: string;

    @Column({ type: "varchar", length: 255, nullable: true })
    Password: string;
}