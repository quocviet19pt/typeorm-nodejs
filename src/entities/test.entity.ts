import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, ManyToOne, Unique, JoinColumn, OneToMany, JoinTable } from "typeorm"
import { Teacher } from "./teacher.entity";
import { Student } from "./student.entity";
import { Student_Test } from "./student_test.entity";

@Entity()
@Unique("unique_test_code", ["Code"])
export class Test {
    @PrimaryGeneratedColumn("increment", {
        primaryKeyConstraintName: "pk_test",
    })
    Id: number;

    @Column({ type: "varchar", length: 255 })
    Code: string;

    @Column({ type: "text", nullable: true })
    Name: string;

    //@ManyToMany(() => Student, (student) => student.Tests)
    @JoinTable({
        name: "student_test",
        joinColumn: { name: "TestId", referencedColumnName: "Id" },
        inverseJoinColumn: { name: " StudentId", referencedColumnName: "Id" },
    })
    Students: Student[];

    @OneToMany(() => Student_Test, (temp) => temp.Tests)
    Student_Tests: Student_Test[];

    @ManyToOne(() => Teacher, (teacher) => teacher.Tests)
    @JoinColumn({ name: "TeacherId" })
    Teachers: Teacher;
}