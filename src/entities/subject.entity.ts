import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, OneToMany, Unique } from "typeorm"
import { Student } from "./student.entity";
import { Teacher } from "./teacher.entity";
import { Student_Subject } from "./student_subject.entity";


@Entity()
@Unique("unique_subject_code", ["Code"])
export class Subject {
    @PrimaryGeneratedColumn("increment", {
        primaryKeyConstraintName: "pk_subject",
    })
    Id: number;

    @Column({ type: "varchar", length: 255 })
    Code: string;

    @Column({ type: "varchar", length: 255, nullable: true })
    Name: string;

    @ManyToMany(() => Student, (student) => student.Subjects)
    Students: Student[];


    @OneToMany(() => Student_Subject, subject => subject.Students)
    Student_Subjects: Student_Subject[];


    @OneToMany(() => Teacher, (teacher) => teacher.Subjects)
    Teachers: Teacher[];

}