
import multer from 'multer';
import * as fs from 'fs';

var dir = __dirname + `/tmp`;
export const initUpload = () => {
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true });
    }
    return multer({
        storage: multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, dir)
            },
            filename: function (req, file, cb) {
                const filename = Date.now() + Math.round(Math.random() * 1E9)
                cb(null, filename + file.originalname)
            }
        })
    })
}

