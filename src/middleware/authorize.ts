import * as jwt from 'express-jwt';
import * as Constants from '../utils/constain';


export default function authorize(roles: string | string[] = []) {

    return [
        // authenticate JWT token and attach user to request object (req.user)
        jwt.expressjwt({
            secret: Constants.JwtSecret,
            requestProperty: "user",
            algorithms: ['HS256'],
            getToken: getToken
        }),
        // authorize based on user role
        (req: any, res: any, next: any) => {
            if (roles.length && !roles.includes(req.user.role)) {
                // user's role is not authorized
                return res.status(401).json({
                    status: false,
                    message: 'Unauthorized'
                });
            }

            // authentication and authorization successful
            next();
        }
        // process authencation with token
    ];
}

const getToken = (req: any) => {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        return req.headers.authorization.split(' ')[1];
    } else if (req.headers.token) {
        return req.headers.token;
    } else if (req.query?.token) {
        return req.query.token;
    }
    return null;
}