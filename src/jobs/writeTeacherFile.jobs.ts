import { AppDataSource } from "../data-source";
import fs from 'fs';
import cron from 'node-cron';
import { Teacher } from "../entities/teacher.entity";


export default async function initTeacherJobs() {
    const teacher = AppDataSource.getRepository(Teacher);
    async function generateReport() {

        const data = await teacher.find();
        const dataString = JSON.stringify(data).replace(/},{/g, '}\n{');


        const filePath = `report_teacher${Date.now()}.txt`;

        fs.appendFile(filePath, dataString, (error) => {
            if (error) {
                console.error('Error writing to file: ', error);
                return;
            }
            console.log(`Report saved to ${filePath}`);
        });

    }

    // Lịch chạy job mỗi ngày vào lúc 10:00 sáng
    cron.schedule('0 10 * * *', () => {
        generateReport();
        console.log('tao file thanh cong')
    });
}