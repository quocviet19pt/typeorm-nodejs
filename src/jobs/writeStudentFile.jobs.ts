import { AppDataSource } from "../data-source";
import fs from 'fs';
import cron from 'node-cron';
import { Student } from "../entities/student.entity";


export default async function initStudentJobs() {
    const student = AppDataSource.getRepository(Student);
    async function generateReport() {

        const data = await student.find();
        const dataString = JSON.stringify(data).replace(/},{/g, '}\n{');


        const filePath = `report_student${Date.now()}.txt`;

        fs.appendFile(filePath, dataString, (error) => {
            if (error) {
                console.error('Error writing to file: ', error);
                return;
            }
            console.log(`Report saved to ${filePath}`);
        });

    }

    // Lịch chạy job mỗi ngày vào lúc 10:00 sáng
    cron.schedule('0 10 * * *', () => {
        generateReport();
        console.log('tao file thanh cong')
    });
}