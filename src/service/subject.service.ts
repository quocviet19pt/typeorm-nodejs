import { AppDataSource } from "../data-source";
import { Subject } from "../entities/subject.entity";

const subject = AppDataSource.getRepository(Subject);

export const findSubjects = async () => {
    try {
        const subjectData = await subject.find();
        return subjectData;
    } catch (e) {
        throw e;
    }
};
export const findSubject = async (Code: string) => {
    try {

        const subjectData = await subject.find(
            {
                where: {
                    Code: Code
                }
            }
        );
        return subjectData;
    } catch (e) {
        throw e;
    }
};

export const createSubject = async (Code: string, Name: string) => {
    try {
        var newData = subject.create({
            Code: Code,
            Name: Name,
        });
        const saveData = await subject.save(newData);

        return saveData;
    } catch (e) {
        throw e;
    }
}

export const updateSubject = async (Id: number, Code: string, Name: string) => {
    try {
        const subjectUpdate = await subject.findOneBy({
            Id: Id
        })
        if (subjectUpdate != null) {
            subjectUpdate.Code = Code;
            subjectUpdate.Name = Name;
            var data = await subject.save(subjectUpdate)
            return {
                status: true,
                messeger: "",
                data: data
            };
        } else {
            return {
                status: false,
                messeger: "Id k ton tai"
            };

        }

    } catch (e) {
        throw e;
    }
}

export const deleteSubject = async (Id: number) => {
    try {
        const subjectDelete = await subject.findOneBy({
            Id: Id,

        })
        if (subjectDelete != null) {

            await subject.remove(subjectDelete)
            return {
                status: true,
                messeger: "",
            };
        } else {
            return {
                status: false,
                messeger: "Id k ton tai"
            };

        }
    } catch (e) {
        throw e;
    }
}

