import { Request, Response } from "express";
import { findStudentTest } from "./student.service";

export const getStudentTest = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var TestId = requestData.TestId;

        const studentTest = await findStudentTest(TestId);
        return res.json({
            status: true,
            data: studentTest,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};
