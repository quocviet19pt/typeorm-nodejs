import { Student_Test } from "../entities/student_test.entity";
import { AppDataSource } from "../data-source";

const student_test = AppDataSource.getRepository(Student_Test);

export const findStudent_Tests = async () => {
    try {
        const student_testData = await student_test.find();
        return student_testData;
    } catch (e) {
        throw e;
    }
};

// export const findStudent_Test = async (StudentId: number) => {
//     try {

//         const student_testData = await student_test.find({
//             relations: {
//                 Tests: true,
//                 Students: true
//             },
//             where: {
//                 Students: {
//                     Id: StudentId
//                 }

//             }
//         }
//         );
//         return student_testData;
//     } catch (e) {
//         throw e;
//     }
// };

export const createStudent_Test = async (StudentId: number, TestId: number, Score: number) => {
    try {
        const saveData = await student_test.save({
            Students: {
                Id: StudentId
            },
            Tests: {
                Id: TestId
            },
            Score: Score
        });
        return saveData;
    } catch (e) {
        throw e;
    }
}

export const updateStudent_Test = async (StudentId: number, TestId: number, Score: number) => {
    try {
        const data = await student_test.save({
            Students: {
                Id: StudentId
            },
            Tests: {
                Id: TestId
            },
            Score: Score
        })
        return {
            status: true,
            messeger: "",
            data: data
        };
    } catch (e) {
        throw e;
    }
}

export const deleteStudent_Test = async (StudentId: number, TestId: number) => {
    try {
        const data = await student_test.delete({
            Students: {
                Id: StudentId
            },
            Tests: {
                Id: TestId
            },

        });
        return {
            status: true,
            messeger: "",
            data: data
        };
    } catch (e) {
        throw e;
    }
}

