import { Request, Response } from "express";
import { findTeacherTest } from "./teacher.service";

export const getTeacherTest = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;

        const teacherTest = await findTeacherTest();
        return res.json({
            status: true,
            data: teacherTest,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};
