import { AppDataSource } from "../data-source";
import { User } from "../entities/user.entity";
import md5 from "md5";
const user = AppDataSource.getRepository(User);

export const findUser = async () => {
    try {
        const testData = await user.find();
        return testData;
    } catch (e) {
        throw e;
    }
};

export const createUser = async (Name: string, Password: string, Role: string) => {
    try {
        var passHash = md5(Password);
        var newData = user.create({
            Name: Name,
            Password: passHash,
            Role: Role
        });

        const saveData = await user.save(newData);

        return saveData;
    } catch (e) {
        throw e;
    }
}

export const updateUser = async (Id: number, Name: string, Password: string, Role: string) => {
    try {
        const userUpdate = await user.findOneBy({
            Id: Id
        })
        if (userUpdate != null) {
            var passHash = md5(Password);
            userUpdate.Name = Name;
            userUpdate.Password = passHash;
            userUpdate.Role = Role;
            var data = await user.save(userUpdate)
            return {
                status: true,
                messeger: "",
                data: data
            };
        } else {
            return {
                status: false,
                messeger: "Id k ton tai"
            };

        }

    } catch (e) {
        throw e;
    }
}

export const deleteUser = async (Id: number) => {
    try {
        const userDelete = await user.findOneBy({
            Id: Id,

        })
        if (userDelete != null) {

            await user.remove(userDelete)
            return {
                status: true,
                messeger: "",
            };
        } else {
            return {
                status: false,
                messeger: "Id k ton tai"
            };

        }
    } catch (e) {
        throw e;
    }
}

