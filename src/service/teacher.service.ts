import { AppDataSource } from "../data-source";
import { Teacher } from "../entities/teacher.entity";

const teacher = AppDataSource.getRepository(Teacher);

export const findTeachers = async () => {
    try {
        const teacherData = await teacher.find({ relations: { Subjects: true } });
        return teacherData;
    } catch (e) {
        throw e;
    }
};
export const findTeacher = async (Code: string) => {
    try {
        const teacherData = await teacher.find(
            {
                relations: {
                    Subjects: true
                },
                where: {
                    Code: Code
                }
            }
        );
        return teacherData;
    } catch (e) {
        throw e;
    }
};

export const findTeacherTest = async () => {
    try {

        const data = await teacher
            .createQueryBuilder("teacher")
            .leftJoinAndSelect("teacher.Tests", "test")
            .where("test.Id is null")
            .getMany()
        return data;
    } catch (e) {
        throw e;
    }
};

export const createTeacher = async (Code: string, Name: string, Exp: number, SubjectId: number, Image: string) => {
    try {
        var newData = teacher.create({
            Code: Code,
            Name: Name,
            Exp: Exp,
            Subjects: {
                Id: SubjectId
            },
            Image: Image
        });
        const saveData = await teacher.save(newData);

        return saveData;
    } catch (e) {
        throw e;
    }
}

export const updateTeacher = async (Id: number, Code: string, Name: string, Exp: number, Image: string) => {
    try {
        const teacherUpdate = await teacher.findOneBy({
            Id: Id
        })
        if (teacherUpdate != null) {
            teacherUpdate.Code = Code;
            teacherUpdate.Name = Name;
            teacherUpdate.Exp = Exp;
            teacherUpdate.Image = Image;

            var data = await teacher.save(teacherUpdate)
            return {
                status: true,
                messeger: "",
                data: data
            };
        } else {
            return {
                status: false,
                messeger: "Id k ton tai"
            };

        }

    } catch (e) {
        throw e;
    }
}

export const deleteTeacher = async (Id: number) => {
    try {
        const teacherDelete = await teacher.findOneBy({
            Id: Id,

        })
        if (teacherDelete != null) {

            await teacher.remove(teacherDelete)
            return {
                status: true,
                messeger: "",
            };
        } else {
            return {
                status: false,
                messeger: "Id k ton tai"
            };

        }
    } catch (e) {
        throw e;
    }
}

