import { Student_Subject } from "../entities/student_subject.entity";
import { AppDataSource } from "../data-source";

const student_subject = AppDataSource.getRepository(Student_Subject);

export const findStudent_Subjects = async () => {
    try {
        const student_subjectData = await student_subject.find();
        return student_subjectData;
    } catch (e) {
        throw e;
    }
};
// export const findStudent_Subject = async (StudentId: number) => {
//     try {

//         const student_subjectData = await student_subject.find(
//             {
//                 select: {
//                     Students: {
//                         Id: true
//                     },
//                     Subjects: {
//                         Id: true
//                     }

//                 },
//                 where: {
//                     Students: {
//                         Id: StudentId
//                     }
//                 }
//             }
//         );
//         return student_subjectData;
//     } catch (e) {
//         throw e;
//     }
// };

export const createStudent_Subject = async (StudentId: number, SubjectId: number) => {
    try {
        const saveData = await student_subject.save({
            Students: {
                Id: StudentId
            },
            Subjects: {
                Id: SubjectId
            },

        });
        return saveData;
    } catch (e) {
        throw e;
    }
}

export const updateStudent_Subject = async (StudentId: number, SubjectId: number) => {
    try {
        const data = await student_subject.save({
            Students: {
                Id: StudentId
            },
            Subjects: {
                Id: SubjectId
            }
        })
        return {
            status: true,
            messeger: "",
            data: data
        };
    } catch (e) {
        throw e;
    }
}

export const deleteStudent_Subject = async (StudentId: number, SubjectId: number) => {
    try {
        const data = await student_subject.delete({
            Students: {
                Id: StudentId
            },
            Subjects: {
                Id: SubjectId
            },

        });
        return {
            status: true,
            messeger: "",
            data: data
        };
    } catch (e) {
        throw e;
    }
}

