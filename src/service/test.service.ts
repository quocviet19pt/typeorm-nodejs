import { AppDataSource } from "../data-source";
import { Test } from "../entities/test.entity";

const test = AppDataSource.getRepository(Test);

export const findTests = async () => {
    try {
        const testData = await test.find({ relations: { Teachers: true } });
        return testData;
    } catch (e) {
        throw e;
    }
};
export const findTest = async (Code: string) => {
    try {
        const testData = await test.find(
            {
                relations: {
                    Teachers: true
                },
                where: {
                    Code: Code
                }
            }
        );
        return testData;
    } catch (e) {
        throw e;
    }
};

export const createTest = async (Code: string, Name: string, TeacherId: number) => {
    try {
        var newData = test.create({
            Code: Code,
            Name: Name,
            Teachers: {
                Id: TeacherId
            }
        });
        const saveData = await test.save(newData);

        return saveData;
    } catch (e) {
        throw e;
    }
}

export const updateTest = async (Id: number, Code: string, Name: string) => {
    try {
        const testUpdate = await test.findOneBy({
            Id: Id
        })
        if (testUpdate != null) {
            testUpdate.Code = Code;
            testUpdate.Name = Name;

            var data = await test.save(testUpdate)
            return {
                status: true,
                messeger: "",
                data: data
            };
        } else {
            return {
                status: false,
                messeger: "Id k ton tai"
            };

        }

    } catch (e) {
        throw e;
    }
}

export const deleteTest = async (Id: number) => {
    try {
        const testDelete = await test.findOneBy({
            Id: Id,

        })
        if (testDelete != null) {

            await test.remove(testDelete)
            return {
                status: true,
                messeger: "",
            };
        } else {
            return {
                status: false,
                messeger: "Id k ton tai"
            };

        }
    } catch (e) {
        throw e;
    }
}

