import { Student } from "../entities/student.entity";
import { AppDataSource } from "../data-source";

const student = AppDataSource.getRepository(Student);

export const findStudents = async () => {
    try {
        const studentData = await student.find();
        return studentData;
    } catch (e) {
        throw e;
    }
};
export const findStudent = async (Code: string, Name: string, Birth: Date) => {
    try {

        const studentData = await student.find(
            {
                where: {
                    Code: Code,
                    Name: Name,
                    Birth: Birth
                }
            }
        );
        return studentData;
    } catch (e) {
        throw e;
    }
};

export const findStudentTest = async (TestId: number) => {
    try {

        const data = await student
            .createQueryBuilder("student")
            .innerJoinAndSelect("student.Student_Tests", "student_test")
            .where("student_test.TestId =:TestId", { TestId: TestId })
            .getOne()
        return data;
    } catch (e) {
        throw e;
    }
};

export const createStudent = async (Code: string, Name: string, Birth: Date, Image: string) => {
    try {
        var newData = student.create({
            Code: Code,
            Name: Name,
            Birth: Birth,
            Image: Image,

        });
        const saveData = await student.save(newData);

        return saveData;
    } catch (e) {
        throw e;
    }
}

export const updateStudent = async (Id: number, Code: string, Name: string, Birth: Date, Image: string) => {
    try {
        const studentUpdate = await student.findOneBy({
            Id: Id
        })
        if (studentUpdate != null) {
            studentUpdate.Code = Code;
            studentUpdate.Name = Name;
            studentUpdate.Birth = Birth;
            studentUpdate.Image = Image;
            var data = await student.save(studentUpdate)
            return {
                status: true,
                messeger: "",
                data: data
            };
        } else {
            return {
                status: false,
                messeger: "Id k ton tai"
            };

        }

    } catch (e) {
        throw e;
    }
}

export const deleteStudent = async (Id: number) => {
    try {
        const studentDelete = await student.findOneBy({
            Id: Id,

        })
        if (studentDelete != null) {

            await student.remove(studentDelete)
            return {
                status: true,
                messeger: "",
            };
        } else {
            return {
                status: false,
                messeger: "Id k ton tai"
            };

        }
    } catch (e) {
        throw e;
    }
}

