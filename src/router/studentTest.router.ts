import express from 'express';
import { getStudentTest } from '../service/report.studentTest';
import authorize from '../middleware/authorize';

const router = express.Router();

router.post('/report/student/get', authorize(["Admin", "User"]), getStudentTest);

export default router;