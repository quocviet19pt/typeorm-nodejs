import express from 'express';
import authorize from '../middleware/authorize';


const router = express.Router();

router.post('/upload/get', authorize(["Admin", "User"]), getStudents);
router.post('/student/create', authorize(["Admin"]), createStudentHandle);
router.post('/student/update', authorize(["Admin"]), updateStudentHandle);
router.post('/student/delete', authorize(["Admin"]), deleteStudentHandle);
export default router;