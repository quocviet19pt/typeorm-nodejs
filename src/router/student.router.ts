import express from 'express';
import { createStudentHandle, deleteStudentHandle, getStudent, getStudents, updateStudentHandle, } from '../controller/student.controller';
import authorize from '../middleware/authorize';
import { initUpload } from '../utils/uploads';


const router = express.Router();

router.post('/student/get', authorize(["Admin", "User"]), getStudents);
router.post('/student/get/code', authorize(["Admin", "User"]), getStudent);
router.post('/student/create', initUpload().single('Image'), authorize(["Admin"]), createStudentHandle);
router.post('/student/update', initUpload().single('Image'), authorize(["Admin"]), updateStudentHandle);
router.post('/student/delete', authorize(["Admin"]), deleteStudentHandle);
export default router;