import express from 'express';
import authorize from '../middleware/authorize';
import { createStudent_TestHandle, deleteStudent_TestHandle, getStudent_Tests, updateStudent_TestHandle } from '../controller/student_test.controller';


const router = express.Router();

router.post('/student_test/get', authorize(["Admin", "User"]), getStudent_Tests);
// router.post('/student_test/get/studentId', authorize(["Admin", "User"]), getStudent_Test);
router.post('/student_test/create', authorize(["Admin"]), createStudent_TestHandle);
router.post('/student_test/update', authorize(["Admin"]), updateStudent_TestHandle);
router.post('/student_test/delete', authorize(["Admin"]), deleteStudent_TestHandle);
export default router;