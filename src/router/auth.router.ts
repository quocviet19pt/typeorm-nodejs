import express, { Request } from 'express';
import { loginUserHandler } from '../controller/auth.controller';

const router = express.Router();


router.post('/login', loginUserHandler);

export default router;
