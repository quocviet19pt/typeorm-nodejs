import express from 'express';
import { createTeacherHandle, deleteTeacherHandle, getTeacher, getTeachers, updateTeacherHandle } from '../controller/teacher.controller';
import authorize from '../middleware/authorize';
import { initUpload } from '../utils/uploads';

const router = express.Router();


router.post('/teacher/create', initUpload().single('Image'), authorize(["Admin"]), createTeacherHandle);
router.post('/teacher/get', authorize(["Admin", "User"]), getTeachers);
router.post('/teacher/get/code', authorize(["Admin", "User"]), getTeacher);
router.post('/teacher/update', initUpload().single('Image'), authorize(["Admin"]), updateTeacherHandle);
router.post('/teacher/delete', authorize(["Admin"]), deleteTeacherHandle);
export default router;