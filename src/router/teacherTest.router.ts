import express from 'express';
import { getTeacherTest } from '../service/report.teacherTest';
import authorize from '../middleware/authorize';

const router = express.Router();

router.post('/report/teacher/get', authorize(["Admin", "User"]), getTeacherTest);

export default router;