import express from 'express';
import { createSubjectHandle, deleteSubjectHandle, getSubject, getSubjects, updateSubjectHandle } from '../controller/subject.controller';
import authorize from '../middleware/authorize';

const router = express.Router();


router.post('/subject/get', authorize(["Admin", "User"]), getSubjects);
router.post('/subject/get/code', authorize(["Admin", "User"]), getSubject);
router.post('/subject/create', authorize(["Admin"]), createSubjectHandle);
router.post('/subject/update', authorize(["Admin"]), updateSubjectHandle);
router.post('/subject/delete', authorize(["Admin"]), deleteSubjectHandle);
export default router;