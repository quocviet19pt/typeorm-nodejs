import express from 'express';
import authorize from '../middleware/authorize';
import { createStudent_SubjectHandle, deleteStudent_SubjectHandle, getStudent_Subjects } from '../controller/student_subject.controller';


const router = express.Router();

router.post('/student_subject/get', authorize(["Admin", "User"]), getStudent_Subjects);
// router.post('/student_subject/get/studentId', authorize(["Admin", "User"]), getStudent_Subject);
router.post('/student_subject/create', authorize(["Admin"]), createStudent_SubjectHandle);
// router.post('/student_subject/update', authorize(["Admin"]), updateStudent_SubjectHandle);
router.post('/student_subject/delete', authorize(["Admin"]), deleteStudent_SubjectHandle);
export default router;