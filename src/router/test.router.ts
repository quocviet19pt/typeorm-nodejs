import express from 'express';
import { createTestHandle, deleteTestHandle, getTest, getTests, updateTestHandle } from '../controller/test.controller';
import authorize from '../middleware/authorize';



const router = express.Router();


router.post('/test/create', authorize(["Admin"]), createTestHandle);
router.post('/test/get', authorize(["Admin", "User"]), getTests);
router.post('/test/get/code', authorize(["Admin", "User"]), getTest);
router.post('/test/update', authorize(["Admin"]), updateTestHandle);
router.post('/test/delete', authorize(["Admin"]), deleteTestHandle);
export default router;