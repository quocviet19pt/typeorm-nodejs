import express from 'express';
import { createUserHandle, deleteUserHandle, updateUserHandle } from '../controller/user.controller';
import authorize from '../middleware/authorize';


const router = express.Router();

router.post('/user/create', authorize(["Admin"]), createUserHandle);
router.post('/user/update', authorize(["Admin"]), updateUserHandle);
router.post('/user/delete', authorize(["Admin"]), deleteUserHandle);
export default router; 