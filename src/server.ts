import express from "express";
import { AppDataSource } from "./data-source";
import studentRouter from "./router/student.router";
import subjectRouter from "./router/subject.router"
import teacherRouter from "./router/teacher.router";
import testRouter from "./router/test.router";
import studentTestRouter from "./router/studentTest.router";
import teacherTestRouter from "./router/teacherTest.router";
import userRouter from "./router/user.router";
import authRouter from "./router/auth.router";
import student_testRouter from "./router/student_test.router";
import student_subjectRouter from "./router/student_subject.router";
import initStudentJobs from "./jobs/writeStudentFile.jobs";
import initTeacherJobs from "./jobs/writeTeacherFile.jobs";


AppDataSource.initialize()
    .then(async () => {
        console.clear()
        let PORT = 7000;
        const app = express();
        app.use(express.json());
        app.use("/api", studentRouter)
        app.use("/api", subjectRouter)
        app.use("/api", teacherRouter)
        app.use("/api", testRouter)
        app.use("/api", studentTestRouter)
        app.use("/api", teacherTestRouter)
        app.use("/api", userRouter)
        app.use("/api", authRouter)
        app.use("/api", student_testRouter)
        app.use("/api", student_subjectRouter)


        initStudentJobs();

        initTeacherJobs();

        const server = app.listen(PORT, () => {
            console.log(`Localhost đã chạy ở port ${PORT}`);
        });
    })
    .catch((error) => console.log(error));