import { DataSource } from "typeorm"
import { Teacher } from "./entities/teacher.entity"
import { Test } from "./entities/test.entity"
import { Student } from "./entities/student.entity"
import { Subject } from "./entities/subject.entity"
import { Student_Test } from "./entities/student_test.entity"
import { Student_Subject } from "./entities/student_subject.entity"
import { User } from "./entities/user.entity"

export const AppDataSource = new DataSource({
    type: "postgres",
    host: "db-devtest-pg.metatech.xyz",
    port: 5432,
    username: "sample",
    password: "tDmgxqFRUm92Q4URItpHdbJKPw4ex4DIdBLL4HQThvfhzUUJZ6",
    database: "sample_db",
    schema: "viet_db",
    synchronize: true,
    // logging: true,
    entities: [Teacher, Test, Student, Subject, Student_Test, Student_Subject, User],
    subscribers: [],
    migrations: [],
})