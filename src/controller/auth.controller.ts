import { Request, Response } from "express";
import md5 from "md5";
import { findUser } from "../service/user.service";
import { JwtSecret, JwtExpiresIn } from "../utils/constain";
import jwt from 'jsonwebtoken';

export const loginUserHandler = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var NameParam = requestData.Name;
        var Password = requestData.Password;

        var user = await findUser();
        var userLogin = user.find(({ Name }) => Name === NameParam);
        if (userLogin == undefined) {
            return res.json({
                status: false,
                message: "ten dang nhap hoac mat khau k dung",
            });
        } else {
            var passWordParamHash = md5(Password);
            var passwordDB = userLogin.Password;
            if (passWordParamHash == passwordDB) {
                const token = jwt.sign({ sub: userLogin.Id, username: userLogin.Name, role: userLogin.Role }, JwtSecret, { expiresIn: JwtExpiresIn });

                return res.json({
                    status: true,
                    message: " dang nhap thanh cong",
                    token: token
                });
            } else {
                return res.json({
                    status: false,
                    message: "password sai",
                });

            }
        }

    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};
