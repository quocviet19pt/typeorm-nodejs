import { Request, Response } from "express";
import { createTest, deleteTest, findTest, findTests, updateTest } from "../service/test.service";

export const getTests = async (req: Request, res: Response) => {
    try {
        const test = await findTests();
        return res.json({
            status: true,
            data: test,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

export const getTest = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Code = requestData.Code;

        const test = await findTest(Code);
        return res.json({
            status: true,
            data: test,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

export const createTestHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Code = requestData.Code;
        var Name = requestData.Name;
        var TeacherId = requestData.TeacherId
        const test = await createTest(Code, Name, TeacherId);
        return res.json({
            status: true,
            data: test,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};


export const updateTestHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Id = requestData.Id;
        var Code = requestData.Code;
        var Name = requestData.Name;

        const result = await updateTest(Id, Code, Name);
        return res.json({
            status: result.status,
            message: result.messeger,
            data: result.data
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

export const deleteTestHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Id = requestData.Id;

        const result = await deleteTest(Id);
        return res.json({
            status: result.status,
            message: result.messeger,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};







