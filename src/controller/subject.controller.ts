import { Request, Response } from "express";
import { createSubject, deleteSubject, findSubject, findSubjects, updateSubject } from "../service/subject.service";

export const getSubjects = async (req: Request, res: Response) => {
    try {
        const subject = await findSubjects();
        return res.json({
            status: true,
            data: subject,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

export const getSubject = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Code = requestData.Code;

        const subject = await findSubject(Code);
        return res.json({
            status: true,
            data: subject,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

export const createSubjectHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Code = requestData.Code;
        var Name = requestData.Name;

        const subject = await createSubject(Code, Name);
        return res.json({
            status: true,
            data: subject,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};


export const updateSubjectHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Id = requestData.Id;
        var Code = requestData.Code;
        var Name = requestData.Name;

        const result = await updateSubject(Id, Code, Name);
        return res.json({
            status: result.status,
            message: result.messeger,
            data: result.data
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

export const deleteSubjectHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Id = requestData.Id;

        const result = await deleteSubject(Id);
        return res.json({
            status: result.status,
            message: result.messeger,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};







