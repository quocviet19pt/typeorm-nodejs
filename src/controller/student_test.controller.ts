import { Request, Response } from "express";
import { createStudent_Test, deleteStudent_Test, findStudent_Tests, updateStudent_Test } from "../service/student_test.service";

export const getStudent_Tests = async (req: Request, res: Response) => {
    try {
        const student_test = await findStudent_Tests();
        return res.json({
            status: true,
            data: student_test,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

// export const getStudent_Test = async (req: Request, res: Response) => {
//     try {
//         var requestData = req.body;
//         var StudentId = requestData.StudentId;

//         const student_test = await findStudent_Test(StudentId)
//         return res.json({
//             status: true,
//             data: student_test,
//         });
//     } catch (e) {
//         var message = "";
//         if (typeof e === "string") message = e
//         else if (e instanceof Error) message = e.message
//         return res.json({
//             status: false,
//             message: message,
//         });
//     }
// };

export const createStudent_TestHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var StudentId = requestData.StudentId;
        var TestId = requestData.TestId;
        var Score = requestData.Score;

        const student_test = await createStudent_Test(StudentId, TestId, Score);
        return res.json({
            status: true,
            data: student_test,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};


export const updateStudent_TestHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var StudentId = requestData.StudentId;
        var TestId = requestData.TestId;
        var Score = requestData.Score;

        const result = await updateStudent_Test(StudentId, TestId, Score);
        return res.json({
            status: result.status,
            message: result.messeger,
            data: result.data
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

export const deleteStudent_TestHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var StudentId = requestData.StudentId;
        var TestId = requestData.TestId;

        const result = await deleteStudent_Test(StudentId, TestId);
        return res.json({
            status: result.status,
            message: result.messeger,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};







