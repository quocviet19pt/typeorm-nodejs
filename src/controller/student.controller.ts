import { Request, Response } from "express";
import { createStudent, deleteStudent, findStudent, findStudents, updateStudent } from "../service/student.service";
import path from "path";
import fs from "fs";

export const getStudents = async (req: Request, res: Response) => {
    try {
        const student = await findStudents();
        return res.json({
            status: true,
            data: student,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

export const getStudent = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Code = requestData.Code;
        var Name = requestData.Name;
        var Birth = requestData.Birth;

        const student = await findStudent(Code, Name, Birth);
        return res.json({
            status: true,
            data: student,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

export const createStudentHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Code = requestData.Code;
        var Name = requestData.Name;
        var Birth = requestData.Birth;
        var Image = ""
        if (req.file) {
            if (!fs.existsSync(path.resolve(__dirname, `public/images`))) {
                fs.mkdirSync(path.resolve(__dirname, `public/images`), { recursive: true });
            }
            const file = req.file;
            // var image_base64 = fs.readFileSync(path.resolve(__dirname, `../tmp/${file.filename}`), "base64");
            // fs.rmSync(path.resolve(__dirname, `../tmp/${file.filename}`), { force: true });

            fs.writeFileSync(path.resolve(__dirname, `public/images/${file.filename}`), file.filename, { flag: 'w', encoding: 'base64' });
            Image = `public/images/${file.filename}`;
        } else {
            Image = "";
        }
        const student = await createStudent(Code, Name, Birth, Image);

        return res.json({
            status: true,
            data: student,
        });

    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};


export const updateStudentHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Id = requestData.Id;
        var Code = requestData.Code;
        var Name = requestData.Name;
        var Birth = requestData.Birth;
        var Image = ""
        if (req.file) {
            if (!fs.existsSync(path.resolve(__dirname, `public/images`))) {
                fs.mkdirSync(path.resolve(__dirname, `public/images`), { recursive: true });
            }
            const file = req.file;
            // var image_base64 = fs.readFileSync(path.resolve(__dirname, `../tmp/${file.filename}`), "base64");
            // fs.rmSync(path.resolve(__dirname, `../tmp/${file.filename}`), { force: true });

            fs.writeFileSync(path.resolve(__dirname, `public/images/${file.filename}`), file.filename, { flag: 'w', encoding: 'base64' });
            Image = `public/images/${file.filename}`;
        } else {
            Image = "Null";
        }

        const result = await updateStudent(Id, Code, Name, Birth, Image);
        return res.json({
            status: result.status,
            message: result.messeger,
            data: result.data
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

export const deleteStudentHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Id = requestData.Id;

        const result = await deleteStudent(Id);
        return res.json({
            status: result.status,
            message: result.messeger,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};







