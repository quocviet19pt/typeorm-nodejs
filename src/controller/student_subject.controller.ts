import { Request, Response } from "express";
import { createStudent_Subject, deleteStudent_Subject, findStudent_Subjects } from "../service/student_subject.service";

export const getStudent_Subjects = async (req: Request, res: Response) => {
    try {
        const student_subject = await findStudent_Subjects();
        return res.json({
            status: true,
            data: student_subject,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

// export const getStudent_Subject = async (req: Request, res: Response) => {
//     try {
//         var requestData = req.body;
//         var StudentId = requestData.StudentId;

//         const student_subject = await findStudent_Subject(StudentId)
//         return res.json({
//             status: true,
//             data: student_subject,
//         });
//     } catch (e) {
//         var message = "";
//         if (typeof e === "string") message = e
//         else if (e instanceof Error) message = e.message
//         return res.json({
//             status: false,
//             message: message,
//         });
//     }
// };

export const createStudent_SubjectHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var StudentId = requestData.StudentId;
        var SubjectId = requestData.SubjectId;

        const student_subject = await createStudent_Subject(StudentId, SubjectId);
        return res.json({
            status: true,
            data: student_subject,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};


export const deleteStudent_SubjectHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var StudentId = requestData.StudentId;
        var SubjectId = requestData.SubjectId;

        const result = await deleteStudent_Subject(StudentId, SubjectId);
        return res.json({
            status: result.status,
            message: result.messeger,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};







