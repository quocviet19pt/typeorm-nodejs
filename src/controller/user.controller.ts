import { Request, Response } from "express";
import { createUser, deleteUser, updateUser } from "../service/user.service";
export const createUserHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Name = requestData.Name;
        var Password = requestData.Password;
        var Role = requestData.Role;

        const user = await createUser(Name, Password, Role);
        return res.json({
            status: true,
            data: user,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};


export const updateUserHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Id = requestData.Id;
        var Name = requestData.Name;
        var Password = requestData.Password;
        var Role = requestData.Role;

        const result = await updateUser(Id, Name, Password, Role);
        return res.json({
            status: result.status,
            message: result.messeger,
            data: result.data
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

export const deleteUserHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Id = requestData.Id;

        const result = await deleteUser(Id);
        return res.json({
            status: result.status,
            message: result.messeger,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};