import { Request, Response } from "express";
import { createTeacher, deleteTeacher, findTeacher, findTeachers, updateTeacher } from "../service/teacher.service";
import path from "path";
import fs from "fs";

export const getTeachers = async (req: Request, res: Response) => {
    try {
        const teacher = await findTeachers();
        return res.json({
            status: true,
            data: teacher,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

export const getTeacher = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Code = requestData.Code;

        const teacher = await findTeacher(Code);
        return res.json({
            status: true,
            data: teacher,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

export const createTeacherHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Code = requestData.Code;
        var Name = requestData.Name;
        var Exp = requestData.Exp;
        var SubjectId = requestData.SubjectId
        var Image = ""
        if (req.file) {
            if (!fs.existsSync(path.resolve(__dirname, `public/images`))) {
                fs.mkdirSync(path.resolve(__dirname, `public/images`), { recursive: true });
            }
            const file = req.file;
            // var image_base64 = fs.readFileSync(path.resolve(__dirname, `../tmp/${file.filename}`), "base64");
            // fs.rmSync(path.resolve(__dirname, `../tmp/${file.filename}`), { force: true });

            fs.writeFileSync(path.resolve(__dirname, `public/images/${file.filename}`), file.filename, { flag: 'w', encoding: 'base64' });
            Image = `public/images/${file.filename}`;
        } else {
            Image = "Null";
        }
        const teacher = await createTeacher(Code, Name, Exp, SubjectId, Image);
        return res.json({
            status: true,
            data: teacher,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};


export const updateTeacherHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Id = requestData.Id;
        var Code = requestData.Code;
        var Name = requestData.Name;
        var Exp = requestData.Exp;
        var Image = ""
        if (req.file) {
            if (!fs.existsSync(path.resolve(__dirname, `public/images`))) {
                fs.mkdirSync(path.resolve(__dirname, `public/images`), { recursive: true });
            }
            const file = req.file;
            // var image_base64 = fs.readFileSync(path.resolve(__dirname, `../tmp/${file.filename}`), "base64");
            // fs.rmSync(path.resolve(__dirname, `../tmp/${file.filename}`), { force: true });

            fs.writeFileSync(path.resolve(__dirname, `public/images/${file.filename}`), file.filename, { flag: 'w', encoding: 'base64' });
            Image = `public/images/${file.filename}`;
        } else {
            Image = "Null";
        }
        const result = await updateTeacher(Id, Code, Name, Exp, Image);
        return res.json({
            status: result.status,
            message: result.messeger,
            data: result.data
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};

export const deleteTeacherHandle = async (req: Request, res: Response) => {
    try {
        var requestData = req.body;
        var Id = requestData.Id;

        const result = await deleteTeacher(Id);
        return res.json({
            status: result.status,
            message: result.messeger,
        });
    } catch (e) {
        var message = "";
        if (typeof e === "string") message = e
        else if (e instanceof Error) message = e.message
        return res.json({
            status: false,
            message: message,
        });
    }
};







